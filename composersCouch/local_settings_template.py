# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'Rzof5I@?AFA0&CpNG[93K-cT'#aLongRandomPasswordGoesHere

# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'composerscouchdb',
        'USER': 'postgres',
        'PASSWORD': 'devDatabase', # database Password goes Here
        'HOST': 'localhost',
        'PORT': '',
        'ATOMIC_REQUESTS': True,
    }
}

HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'haystack.backends.solr_backend.SolrEngine',
        'URL': 'http://127.0.0.1:8983/solr',
        'INCLUDE_SPELLING': True,
    },
}

FEEDLY_REDIS_CONFIG = {
    'default': {
        'host': 'localhost',
        'port': 6379,
        'password': '',# Redis Password goes Here (empty for dev)
        'db': 0
    },
}

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '127.0.0.1:11211',
    }
}

#Email
if DEBUG:
    EMAIL_HOST = 'localhost'
    EMAIL_PORT = 1025
    EMAIL_HOST_USER = ''
    EMAIL_HOST_PASSWORD = ''
    EMAIL_USE_TLS = False
    DEFAULT_FROM_EMAIL = 'testing@example.com'
EMAIL_BACKEND = 'django.core.mail.backends.dummy.EmailBackend'
EMAIL_HOST_USER = ''#AnEmailAddressGoesHere
EMAIL_HOST_PASSWORD = ''#PasswordToSaidEmailAdress
EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587

# Social Auth
TWITTER_CONSUMER_KEY         = 't64bvuxy0triEzEnHcyg'
TWITTER_CONSUMER_SECRET      = 'jm41BJqDger9veDu3Aa7jswN4ZgQ9yIktlZIY4cSps'
FACEBOOK_APP_ID              = '525965254182714'
FACEBOOK_API_SECRET          = 'bdd9cdd707d80d08bd53660852b91c51'
GOOGLE_OAUTH2_CLIENT_ID      = '566838544572.apps.googleusercontent.com'
GOOGLE_OAUTH2_CLIENT_SECRET  = 'BpAQ6KT37BLQxNc5ETzC0sMS'
