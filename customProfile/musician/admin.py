from django.contrib import admin

from models import Instrument, Member


admin.site.register(Instrument)
admin.site.register(Member)
